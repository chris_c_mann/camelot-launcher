(ns camelot.launcher.common.json-test
  (:require [camelot.launcher.common.json :as sut]
            [cljs.test :refer [deftest testing is] :include-macros true]))

(deftest test-from-json
  (testing "from-json"
    (testing "should keywordize keys recursively"
      (is (= (sut/from-json #js {"hello" #js {"world" 42}})
             {:hello {:world 42}})))

    (testing "should keywordize values for :type"
      (is (= (sut/from-json #js {"type" "some-event-type"})
             {:type :some-event-type})))

    (testing "should keywordize event correctly for process-state-change-request-event"
      (is (= (sut/from-json #js {"type" "process-state-change-request-event"
                                 "target-state" "started"})
             {:type :process-state-change-request-event
              :target-state :started})))

    (testing "should keywordize values for process-state-change-event"
      (is (= (sut/from-json #js {"type" "process-state-change-event"
                                 "new-state" "starting"
                                 "old-state" "stopped"})
             {:type :process-state-change-event
              :new-state :starting
              :old-state :stopped})))

    (testing "should keywordize values for :process-state-change-failure-event"
      (is (= (sut/from-json #js {"type" "process-state-change-failure-event"
                                 "error" #js {"type" "bad-error-type"
                                              "message" "Reason"}
                                 "current-state" "starting"
                                 "target-state" "stopped"})
             {:type :process-state-change-failure-event
              :error {:type :bad-error-type
                      :message "Reason"}
              :current-state :starting
              :target-state :stopped})))))
