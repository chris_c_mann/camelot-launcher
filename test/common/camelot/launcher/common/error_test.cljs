(ns camelot.launcher.common.error-test
  (:require [camelot.launcher.common.error :as sut]
            [cljs.test :refer [deftest testing is] :include-macros true]))

(deftest test-stringify-error-chain
  (testing "stringify-error-chain"
    (testing "should produce the expected string for an error without a cause"
      (is (= "Some stack"
             (sut/stringify-error-chain #js {"stack" "Some stack"
                                             "message" "A message"
                                             "name" "FakeError"}))))

    (testing "should produce the expected string for an error with a cause"
      (let [cause #js {"stack" "Cause stack"
                       "message" "A message"
                       "name" "FakeError"}]
        (is (= "Rethrowers stack\nCaused by Cause stack"
               (sut/stringify-error-chain #js {"stack" "Rethrowers stack"
                                               "message" "A message"
                                               "name" "FakeError"
                                               "cause" cause})))))

    (testing "should bound the number of exceptions unrolled"
      (let [error #js {"stack" "Cause stack"
                       "message" "A message"
                       "name" "FakeError"}]
        (aset error "cause" error)
        (is (= (count (re-seq #"Cause stack" (sut/stringify-error-chain error))) 10))
        (is (.endsWith (sut/stringify-error-chain error) "<limit reached>"))))))
