(ns camelot.launcher.config.format-test
  (:require [camelot.launcher.config.format :as sut]
            [cljs.test :refer [deftest testing is] :include-macros true]))

(deftest transform-test
  (testing "transform"
    (testing "should transform http-port from a string"
      (let [config {:server {:http-port "8080"}}
            expected {:server {:http-port 8080}}]
        (is (= expected (sut/transform config)))))

    (testing "should leave http-port as a number unchanged"
      (let [config {:server {:http-port 8080}}
            expected {:server {:http-port 8080}}]
        (is (= expected (sut/transform config)))))

    (testing "should transform language from a string"
      (let [config {:language "en"}
            expected {:language :en}]
        (is (= expected (sut/transform config)))))

    (testing "should transform species name style from a string"
      (let [config {:species-name-style "scientific"}
            expected {:species-name-style :scientific}]
        (is (= expected (sut/transform config)))))

    (testing "jvm-extra-args"
      (testing "should be elided if empty"
        (let [config {:server {:jvm-extra-args ""}}
              expected {}]
          (is (= expected (sut/transform config)))))

      (testing "jvm-extra-args should not elided if defined"
        (let [config {:server {:jvm-extra-args "-Xmx1g"}}
              expected {:server {:jvm-extra-args "-Xmx1g"}}]
          (is (= expected (sut/transform config))))))

    (testing "max-heap-size"
      (testing "should be elided if empty"
        (let [config {:server {:max-heap-size ""}}
              expected {}]
          (is (= expected (sut/transform config)))))

      (testing "jvm-extra-args should not elided if defined"
        (let [config {:server {:max-heap-size "1024"}}
              expected {:server {:max-heap-size 1024}}]
          (is (= expected (sut/transform config))))))))
