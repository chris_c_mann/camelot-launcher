(ns camelot.launcher.ui.core-test
  (:require [camelot.launcher.ui.core :as sut]
            [cljs.test :refer [deftest testing is] :include-macros true]))

(def system
  {:run-cli! (constantly :cli)
   :run-gui! (constantly :gui)
   :ui/config {:config (atom {})}})

(defn set-config
  [s m]
  (assoc-in s [:ui/config :config] (atom m)))

(deftest test-select-ui
  (testing "select-ui"
    (testing "should use cli if \"cli\" set as ui-mode"
      (let [s (set-config system {:launcher {:ui-mode :cli}})]
        (is (= (sut/select-ui s) :cli))))

    (testing "should use gui if \"gui\" set as ui-mode"
      (let [s (set-config system {:launcher {:ui-mode :gui}})]
        (is (= (sut/select-ui s) :gui))))

    (testing "should use gui if no ui-mode set"
      (is (= (sut/select-ui system) :gui)))

    (testing "should throw if an invalid ui-mode is set"
      (let [s (set-config system {:launcher {:ui-mode :xyz}})]
        (is (thrown? js/Error (sut/select-ui s)))))

    (testing "should throw if a ui-mode is specified but nil"
      (let [s (set-config system {:launcher {:ui-mode nil}})]
        (is (thrown? js/Error (sut/select-ui s)))))))
