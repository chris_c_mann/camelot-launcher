(ns camelot.launcher.ui.cli.core-test
  (:require [camelot.launcher.ui.cli.core :as sut]
            [cljs.test :refer [deftest testing is] :include-macros true]))

(deftest test-command-handler
  (testing "command-handler"
    (testing "should create launch-quit-event on 'quit' command"
      (is (= (sut/command-handler system "quit") [{:type :launcher-quit-event}])))

    (testing "should create expected event on 'start' command"
      (is (= (sut/command-handler system "start")
             [{:type :process-state-change-request-event
               :target-state :started}])))

    (testing "should create expected event on 'stop' command"
      (is (= (sut/command-handler system "stop")
             [{:type :process-state-change-request-event
               :target-state :stopped}])))

    (testing "Should output status on 'status' command"
      (with-redefs [print-call (atom nil)
                    print #(reset! print-call %)
                    camelot.launcher.ui.action.status/summary-str
                    (constantly "<process status>")]
        (is (= (sut/command-handler system "status") []))
        (is (= @print-call "<process status>"))))))
