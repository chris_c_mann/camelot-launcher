# Docker image for CI

FROM clojure:openjdk-11-tools-deps
MAINTAINER Chris Mann <chris@bitpattern.com.au>

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get update -y
RUN apt-get install nodejs npm curl -y

WORKDIR /tmp
COPY tests.edn /tmp/
COPY shadow-cljs.edn /tmp/
COPY deps.edn /tmp/
COPY src /tmp/src/
COPY resources /tmp/resources/
COPY script /tmp/script
COPY test /tmp/test

ENV PATH="/root/bin:$PATH"

RUN npm install
RUN npm install -g shadow-cljs

RUN script/build
RUN rm -rf /tmp/*
