(ns camelot.launcher.environ.core
  "Integration with the broader environment.

This is a bit of a catch-all; things may be split out to dedicated modules
from here as needed."
  (:require [camelot.launcher.config.core :as config]
            [camelot.launcher.event.core :as event]
            [cljs.core.async :as async :include-macros true]
            [goog.string :as gstr]
            [clojure.string :as cstr]
            [integrant.core :as ig]
            goog.string.format))

(def module-key :environ)
(def module (hash-map module-key {:environ/config (ig/ref config/module-key)
                                  :environ/event (ig/ref event/module-key)}))

(defonce child-process (js/require "child_process"))
(defonce process (js/require "process"))
(defonce readline (js/require "readline"))

(defn- get-config
  [system k]
  (get @(-> system :environ/config :config) k))

(defn- max-heap-size-arg
  [system]
  (let [max-heap-size (:max-heap-size (get-config system :server))]
    (when max-heap-size
      (gstr/format "-Xmx%dm" max-heap-size))))

(defn java-configuration
  [system]
  (.spawnSync child-process
              (get-config system :java-command)
              (clj->js (vec (remove nil? [(max-heap-size-arg system)
                                          "-XX:+PrintFlagsFinal"
                                          "-version"])))))

(defn readline-prompt
  [prompt handler]
  (let [rl (.createInterface readline #js {"input" (.-stdin process)
                                           "output" (.-stdout process)
                                           "prompt" prompt})]
    (.prompt rl)
    (.on rl "line" (fn [cmd]
                     (if (= cmd "")
                       (.close rl)
                       (do
                         (handler ((fnil cstr/trim "") cmd))
                         (.prompt rl)))))))

(defn- handle-event
  [event]
  (condp = (:type event)
    :launcher-quit-event
    (.exit process 0)

    nil))

(defn- start-listener!
  [system]
  (let [ch (::broadcast-channel-rx system)]
    (async/go-loop []
      (let [event (async/<! ch)]
        (handle-event event))
      (recur))
    ch))

(defmethod ig/prep-key module-key [_ system]
  (assoc system ::broadcast-channel-tx (async/chan)))

(defmethod ig/init-key module-key [_ system]
  (let [broadcast-ch-rx ((-> system :environ/event :register!)
                         (::broadcast-channel-tx system))
        s (assoc system
                 ::broadcast-channel-rx broadcast-ch-rx
                 :java-configuration #(java-configuration system)
                 :readline-prompt readline-prompt
                 :platform #(.-platform process))]
    (start-listener! s)
    s))
