(ns camelot.launcher.ui.core
  (:require [camelot.launcher.ui.cli.core :as cli]
            [camelot.launcher.ui.gui.core :as gui]
            [camelot.launcher.process.core :as process]
            [camelot.launcher.environ.core :as environ]
            [camelot.launcher.event.core :as event]
            [camelot.launcher.electron.core :as electron]
            [camelot.launcher.config.core :as config]
            [integrant.core :as ig]))

(def module-key :ui)
(def module (hash-map module-key {:ui/config (ig/ref config/module-key)
                                  :ui/environ (ig/ref environ/module-key)
                                  :ui/event (ig/ref event/module-key)
                                  :ui/electron (ig/ref electron/module-key)
                                  :ui/process (ig/ref process/module-key)}))

(defn- get-config
  [system k]
  (get @(-> system :ui/config :config) k))

(defn select-ui
  [system]
  (let [launcher (get-config system :launcher)]
    (if (contains? launcher :ui-mode)
      (condp = (:ui-mode launcher)
        :cli ((:run-cli! system))

        :gui ((:run-gui! system))

        (throw (ex-info "UI mode not known" (select-keys launcher [:ui-mode]))))
      ((:run-gui! system)))))

(defmethod ig/init-key module-key [_ system]
  (try
    (let [s (assoc system
                   :run-cli! (fn [] (cli/run-ui! system) :cli)
                   :run-gui! (fn [] (gui/run-ui! system) :gui))]
      (assoc s :style (select-ui s)))
    (catch js/Error e
      (.log js/console e))))
