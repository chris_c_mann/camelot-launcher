(ns camelot.launcher.ui.cli.core
  "Configuration management for Camelot."
  (:require [camelot.launcher.common.events :as common-events]
            [camelot.launcher.ui.action.status :as ui-status]
            [cljs.core.async :as async :include-macros true]))

(defn command-handler
  [system cmd]
  (condp = cmd
    "quit" [(common-events/launcher-quit)]
    "start" [(common-events/server-state-change-request :started)]
    "stop" [(common-events/server-state-change-request :stopped)]
    "status" (do (print (ui-status/summary-str system)) [])
    nil))

(defn run-ui!
  [system]
  (let [tx-ch (async/chan)
        rx-ch ((-> system :ui/event :register!) tx-ch)
        p (-> system :ui/environ :readline-prompt)]
    (p "> " #(doseq [evt (command-handler system %)]
               (async/put! tx-ch evt)))))
