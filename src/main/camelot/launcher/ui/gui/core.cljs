(ns camelot.launcher.ui.gui.core
  (:require [camelot.launcher.common.log :as log]
            [camelot.launcher.common.events :as common-events]
            [cljs.core.async :as async :include-macros true]))

(def window-config #js {"width" 1240
                        "height" 768
                        "webPreferences" #js {"nodeIntegration" true}})

(def renderer-ipc-channel "renderer")
(def main-ipc-channel "main")

(defn- finalise-ipc-init
  [system]
  (let [send-ipc! (-> system :ui/electron :send-ipc!)
        event {:type :ipc-initialised}]
    (log/log :debug "=> IPC: " event)
    (send-ipc! main-ipc-channel event)))

(defn- publish-ipc-to-broadcast-channel!
  [system tx-ch]
  (let [on (-> system :ui/electron :on-ipc!)]
    (on renderer-ipc-channel #(do
                                (log/log :debug "IPC => BROADCAST: " %1)
                                (async/put! tx-ch %1)))))

(defn- publish-events-to-ipc!
  [system]
  (let [tx-ch (async/chan)
        rx-ch ((-> system :ui/event :register!) tx-ch)]
    (async/go
      (loop []
        (let [event (async/<! rx-ch)
              send-ipc! (-> system :ui/electron :send-ipc!)]
          (log/log :debug "BROADCAST => IPC: " event)
          (send-ipc! main-ipc-channel event))
        (recur)))
    tx-ch))

(defn- event<->ipc
  [system]
  (let [tx-ch (publish-events-to-ipc! system)]
    (publish-ipc-to-broadcast-channel! system tx-ch)
    (finalise-ipc-init system)))

(defn run-ui!
  [system]
  (let [create-window! (-> system :ui/electron :create-window!)]
    (create-window! window-config (memoize #(event<->ipc system)))))
