(ns camelot.launcher.event.core
  (:require [camelot.launcher.common.log :as log]
            [camelot.launcher.common.events :as common-events]
            [cljs.core.async :as async :include-macros true]
            [clojure.spec.alpha :as s]
            [integrant.core :as ig]))

(def module-key :event)
(def module (hash-map module-key {}))

(def ^:private channels (atom {}))

(defn- broadcast-loop!
  [system]
  (async/go
    (loop []
      (let [[v port] (async/alts! (into [] (keys @channels)))]
        (when-not (= port (::update-channels-chan system))
          (if (s/valid? ::common-events/event v)
            (if-let [sender (get @channels port)]
              (do
                (log/log :debug "BROADCASTING EVENT: " v)
                (doall (map #(when-not (= % port) (async/put! % v))
                            (remove #(or (nil? %) (= sender %))
                                    (vals @channels)))))
              (log/log :error "Unknown sender: " port))
            (do
              (log/log :error "Invalid event received:")
              (s/explain ::common-events/event v)))))
      (recur))))

(defn- update-channels!
  [system rx-ch f]
  (let [tx-ch (async/chan)]
    (swap! channels f rx-ch tx-ch)
    (async/put! (::update-channels-chan system) true)
    tx-ch))

(defn register!
  [system rx-ch]
  (update-channels! system rx-ch assoc))

(defn deregister!
  [system rx-ch]
  (update-channels! system rx-ch dissoc))

(defmethod ig/prep-key module-key [_ system]
  (let [ch (async/chan)]
    (swap! channels assoc ch nil)
    (assoc system ::update-channels-chan ch)))

(defmethod ig/init-key module-key [_ system]
  (broadcast-loop! system)
  (assoc system
         :register! #(register! system %)
         :deregister! #(deregister! system %)))
