(ns camelot.launcher.electron.core
  (:require [camelot.launcher.common.json :as json]
            [camelot.launcher.environ.core :as environ]
            [integrant.core :as ig]))

(def module-key :electron)
(def module (hash-map module-key {:electron/environ (ig/ref environ/module-key)}))

(def main-window (atom nil))

(defn- electron
  []
  (js/require "electron"))

(defn- app
  []
  (.-app (electron)))

(defn shell
  []
  (.-shell (electron)))

(defn menu
  []
  (.-Menu (electron)))

(defn build-menu-template
  [win]
  (let [base [{:label "File"
               :submenu [{:label "Quit"
                          :accelerator "CmdOrCtrl+Q"
                          :click #(.quit (app))}]}
              {:label "Edit"
               :submenu [{:label "Undo"
                          :accelerator "CmdOrCtrl+Z"
                          :selector "undo:"}
                         {:label "Redo"
                          :accelerator "Shift+CmdOrCtrl+Z"
                          :selector "redo:"}
                         {:label "Cut"
                          :accelerator "CmdOrCtrl+X"
                          :selector "cut:"}
                         {:label "Copy"
                          :accelerator "CmdOrCtrl+C"
                          :selector "copy:"}
                         {:label "Paste"
                          :accelerator "CmdOrCtrl+V"
                          :selector "paste:"}
                         {:label "Select All"
                          :accelerator "CmdOrCtrl+A"
                          :selector "selectAll:"}]}]]

    (if (.-DEBUG js/goog)
      (conj base {:label "View"
                  :submenu [{:label "Toggle Dev Tools"
                             :accelerator "F12"
                             :click #(.. win -webContents toggleDevTools)}]})
      base)))

(defn- on-ready
  [browser-window window-config finish-load-handler]
  (reset! main-window (browser-window. window-config))

  (let [mt (clj->js (build-menu-template ^js/electron.BrowserWindow @main-window))]
    (.setApplicationMenu (menu) (.buildFromTemplate (menu) mt)))

  (.loadFile ^js/electron.BrowserWindow @main-window
             "www/index.html")

  (.on (.-webContents ^js/electron.BrowserWindow @main-window)
       "new-window"
       (fn [e url frame-name disposition options]
         (.preventDefault e)
         (let [success? (.openExternal (shell) url)]
           ;; fallback in event external program could be opened
           (when-not success?
             (let [browser-window (.-BrowserWindow (electron))
                   win (browser-window. #js {"webContents" (.-webContents options)
                                             "webPreferences" #js {"nodeIntegration" true}
                                             "show" false})]
               (.once win "ready-to-show" #(.show win))
               (when-not (.-webContents options)
                 (.loadURL win url))
               (set! (.-newGuest e) win))))))

  (.on ^js/electron.BrowserWindow @main-window
       "closed" #(reset! main-window nil))

  (let [wc (.-webContents @main-window)]
    (.on wc "did-finish-load" #(finish-load-handler wc))))

(defn create-window!
  [system window-config finish-load-handler]
  (let [elec (electron)
        app (.-app elec)
        browser-window (.-BrowserWindow elec)]

    (.on app "window-all-closed"
         #(.quit app))

    (.on app "ready"
         #(on-ready browser-window window-config finish-load-handler))))

(defn on-ipc!
  [ipc-ch-name f]
  (.on (.-ipcMain (electron))
       ipc-ch-name #(f (json/from-json %2))))

(defn send-ipc!
  [ipc-ch-name o]
  (let [wc (.-webContents @main-window)]
    (.send wc ipc-ch-name (json/to-json o))))

(defmethod ig/init-key module-key [_ system]
  (assoc system
         :create-window! #(create-window! system %1 %2)
         :on-ipc! on-ipc!
         :send-ipc! send-ipc!))
