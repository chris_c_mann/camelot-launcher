(ns camelot.launcher.process.runtime
  (:refer-clojure :exclude [apply])
  (:require [httpurr.client.node :as http]
            [clojure.string :as cstr]
            [promesa.core :as p]
            [goog.string :as gstr]
            [goog.object :as obj]
            goog.string.format))

(defn- java-configuration
  [system]
  ((-> system :process/environ :java-configuration)))

(defn- find-flag-configuration
  [output flag process]
  (some->> output
           cstr/split-lines
           (filter #(re-find (re-pattern (str "\\s" flag "\\s")) %))
           first
           (re-find #"=\s*([^ \t]+)")
           second
           process))

(defn- java-details
  [system]
  (let [result (java-configuration system)]
    (if (zero? (obj/get result "status"))
      {:max-heap-size (find-flag-configuration (obj/get result "stdout") "MaxHeapSize" long)
       :version (first (cstr/split-lines (.toString (obj/get result "stderr"))))}
      {:error (or (obj/get result "error")
                  (obj/get result "stderr")
                  "Failed to get Java details. Please check your java command and JVM arguments.")})))

(defn- request
  [config opts]
  (http/get (gstr/format "http://localhost:%d/runtime"
                         (get-in config [:server :http-port]))
              opts))

(defn- parse
  [system response]
  (let [status (:status response)
        body (js->clj (.parse js/JSON (:body response)))]
    (let [details (java-details system)]
      (when (= status 200)
        {:alternate-urls (get body "alternate-urls")
         :java details}))))

(defn- handle-request
  [system publish!]
  (let [config @(-> system :process/config :config)]
    (try
      (-> (request config {:timeout 2000})
          (p/then publish!)
          (p/catch (fn [e] (publish! {}))))
      (catch js/Error e
        (publish! {})))))

(defn publish-info
  [system publish!]
  (handle-request system #(-> (parse system %) publish!)))
