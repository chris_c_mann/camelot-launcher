(ns camelot.launcher.process.status
  (:refer-clojure :exclude [ref])
  (:require [clojure.string :as cstr]))

(def ^:private process-state-descriptions
  {:starting "Starting..."
   :stopping "Stopping..."
   :started "Running"
   :stopped "Stopped"
   :unknown "Unknown..."})

(defn- ref
  [system]
  (get system ::status))

(defn- current
  [system]
  @(ref system))

(defn- next-process-state
  [{:keys [::process-state]} {:keys [healthy?]}]
  (cond
    (and (not healthy?) (= process-state :starting))
    :starting

    (and healthy? (= process-state :stopping))
    :stopping

    healthy?
    :started

    :default
    :stopped))

(defn- next-version-status
  [cur nps healthcheck]
  (when-not (= nps :stopped)
    (or (:software-version healthcheck)
        (::software-version cur))))

(defn process-state
  [system]
  (-> system current ::process-state))

(defn describe-process-state
  [system]
  (let [{:keys [::process-state]} (current system)]
    (process-state-descriptions process-state)))

(defn software-version
  [system]
  (::software-version (current system)))

(defn database-version
  [system]
  (first (cstr/split (::database-version (current system)) #"[-_]")))

(defn- next-status
  [cur-status healthcheck]
  (let [nps (next-process-state cur-status healthcheck)
        nvs (next-version-status cur-status nps healthcheck)]
    (if nvs
      {::process-state nps
       ::software-version nvs
       ::database-version (:database-version healthcheck)}
      {::process-state nps})))

(defn current-status
  [system]
  {:process-state (process-state system)
   :process-state-description (describe-process-state system)
   :software-version (software-version system)
   :database-version (database-version system)})

(defn apply-healthcheck!
  [system healthcheck]
  (let [cur (current system)
        next (next-status cur healthcheck)]
    (reset! (ref system) next)
    (when (not= (::process-state cur) (::process-state next))
      {:old-state (::process-state cur)
       :new-state (::process-state next)})))

(defn enter-starting-process-state!
  [system]
  (let [cur (current system)]
    (swap! (ref system) assoc ::process-state :starting)
    {:old-state (::process-state cur)
     :new-state :starting}))

(defn enter-stopping-process-state!
  [system]
  (let [cur (current system)]
    (swap! (ref system) assoc ::process-state :stopping)
    {:old-state (::process-state cur)
     :new-state :stopping}))

(defn assoc-initial
  [system]
  (assoc system ::status (atom {::process-state :unknown})))

(defn stopped?
  [system]
  (= (process-state system) :stopped))

(defn started?
  [system]
  (= (process-state system) :started))

(defn intermediate-process-state?
  [system]
  (not (or (started? system) (stopped? system))))
