(ns camelot.launcher.process.darwin)

(def path (js/require "path"))
(def process (js/require "process"))

(def darwin-packaging-path
  "Resources/app")

(defn- find-in-hierarchy
  "Darwin-specific search for paths."
  [find-fn d]
  (if (find-fn d)
    d
    (let [resource-d (.join path d darwin-packaging-path)]
      (if (find-fn resource-d)
        resource-d
        (let [parent (.dirname path d)]
          (when-not (or (= d "/") (= d parent))
            (find-in-hierarchy find-fn parent)))))))

(defn darwin?
  []
  (= (.-platform process) "darwin"))

(defn find-in-exec-path
  "MacOS runs the app with the cwd of '/' so we go on a little search."
  [find-fn]
  (find-in-hierarchy find-fn (.dirname path (.-execPath process))))
