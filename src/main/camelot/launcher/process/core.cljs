(ns camelot.launcher.process.core
  "Process management for Camelot."
  (:require [camelot.launcher.common.log :as log]
            [camelot.launcher.config.core :as config]
            [camelot.launcher.event.core :as event]
            [camelot.launcher.environ.core :as environ]
            [camelot.launcher.process.status :as status]
            [camelot.launcher.process.runtime :as runtime]
            [camelot.launcher.process.event :as process-event]
            [camelot.launcher.process.healthcheck :as healthcheck]
            [camelot.launcher.process.command :as command]
            [cljs.core.async :as async :include-macros true]
            [integrant.core :as ig]))

(def module-key :process)
(def module (hash-map module-key {:process/config (ig/ref config/module-key)
                                  :process/event (ig/ref event/module-key)
                                  :process/environ (ig/ref environ/module-key)}))

(defn- get-config
  [system]
  @(-> system :process/config :config))

(defn- stop!
  [system]
  (when-not (status/stopped? system)
    (let [change (status/enter-stopping-process-state! system)]
      (command/stop! (get-config system))
      (async/put! (::broadcast-channel-tx system)
                  (process-event/server-state-change change)))))

(defn- broadcast-start-failure
  [system code]
  (async/put! (::broadcast-channel-tx system)
              (process-event/server-state-change-failure
               (str (if (= (status/process-state system) :stopped)
                      "Camelot could not be started. "
                      "Camelot did not shutdown cleanly. ")
                    "Please check the system logs for details."))))

(defn- start-process-close-handler
  [system code]
  (stop! system)
  (when (or (nil? code) (pos-int? code))
    (broadcast-start-failure system code))
  (log/log (if (zero? code) :info :error)
           "Exited with status: " code))

(defn- start!
  [system]
  (when-not (status/started? system)
    (if (command/start! (get-config system)
                        (partial start-process-close-handler system))
      (let [change (status/enter-starting-process-state! system)]
        (async/put! (::broadcast-channel-tx system)
                    (process-event/server-state-change change)))
      (broadcast-start-failure system nil))))

(defn- start-broadcast-listener!
  [system]
  (let [ch (::broadcast-channel-rx system)]
    (async/go-loop []
      (let [v (<! ch)]
        (condp = (:type v)
          :process-state-notification-request-event
          (async/>! (::broadcast-channel-tx system)
                    (process-event/server-state-notification (status/process-state system)))

          :process-state-change-request-event
          (condp = (:target-state v)
            :started
            (start! system)

            :stopped
            (stop! system)

            nil)

          :process-runtime-info-notification-request-event
          (runtime/publish-info
           system
           (fn [info]
             (async/put! (::broadcast-channel-tx system)
                         (process-event/server-runtime-info
                          (merge (status/current-status system) info)))))
          nil)
        (recur)))))

(defmethod ig/prep-key module-key [_ system]
  (status/assoc-initial system))

(defmethod ig/init-key module-key [_ system]
  (let [broadcast-ch-tx (async/chan)
        broadcast-ch-rx ((-> system :process/event :register!) broadcast-ch-tx)]
    (let [s (assoc system
                   ::broadcast-channel-tx broadcast-ch-tx
                   ::broadcast-channel-rx broadcast-ch-rx
                   :status #(status/current-status system))]
      (healthcheck/start-listener! s broadcast-ch-tx )
      (start-broadcast-listener! s)
      s)))
