(ns camelot.launcher.process.command
  (:require [camelot.launcher.process.darwin :as darwin]
            [camelot.launcher.common.log :as log]
            [cljs.core.async :as async :include-macros true]
            [clojure.string :as cstr]
            [httpurr.client.node :as http]
            [promesa.core :as p]
            [goog.string :as gstr]
            goog.string.format))

(def fs (js/require "fs"))
(def path (js/require "path"))
(def child-process (js/require "child_process"))

(def application-not-found-error
  "Camelot application jar could not be found. Double check the configured
  installation location.")

(defn find-file
  [file-re p]
  (try
    (some->> p
             (.readdirSync fs)
             (filter #(try (and (.isFile (.statSync fs (.join path p %)))
                                (re-find file-re %))
                           (catch js/Error e
                             false)))
             first)
    (catch js/Error e
      nil)))

(defn find-camelot-jar
  [p]
  (find-file #"^camelot.*.jar$" p))

(defn- normalise-app-path
  "Search up from the execPath of the process, if we need to search for the jar."
  [find-fn p]
  (if (and (darwin/darwin?) (= p ".") (not (find-fn p)))
    (darwin/find-in-exec-path find-fn)
    p))

(defn- file-in-path
  [find-fn p]
  (let [adf (normalise-app-path find-fn p)]
    (some->> adf
             (find-fn)
             (.join path adf))))

(defn- camelot-jar-path
  [p]
  (file-in-path find-camelot-jar p))

(defn- max-heap-size-arg
  [config]
  (let [{:keys [max-heap-size]} (:server config)]
    (if max-heap-size
      (gstr/format "-Xmx%dm" max-heap-size)
      "")))

(defn- build-jvm-args
  [config camelot-jar]
  (let [{:keys [jvm-extra-args max-heap-size]} (:server config)
        logpath (-> config :paths :log)]
    (vec (remove empty? (concat
                         (cstr/split (or jvm-extra-args "") #"\s+")
                         [(str "-Dcamelot.log.path=" logpath)
                          (max-heap-size-arg config)
                          "-jar"
                          camelot-jar])))))

(defn- java-version
  [config]
  (js->clj (.spawnSync child-process (:java-command config) #js ["-version"])))

(defn- java-server-vm?
  [version-data]
  (not (cstr/includes? (get version-data "stderr") "Client VM")))

(defn- java-operable?
  [version-data]
  (zero? (get version-data "status")))

(defn start!
  [config close-handler]
  (let [version-data (java-version config)]
    (cond
      (not (java-operable? version-data))
      (do
        (log/log :error (str "ERROR: Command '" (:java-command config) "' could not be found"))
        false)

      (not (java-server-vm? version-data))
      (do
        (log/log :error (str "ERROR: 32-bit Java detected. Please use a 64-bit (\"x64\") Java from"
                             " https://www.oracle.com/java/technologies/javase-jre8-downloads.html"))
        false)

      :else
      (if-let [camelot-jar (camelot-jar-path (get-in config [:paths :application]))]
        (let [p (.spawn child-process
                        (:java-command config)
                        (clj->js (build-jvm-args config camelot-jar))
                        #js {"detached" true})]
          (.on (.-stdout p) "data" (fn [data] (log/log :info (.toString data))))
          (.on (.-stderr p) "data" (fn [data] (log/log :error (.toString data))))
          (.on p "close" close-handler)
          true)
        (do
          (log/log :error "ERROR: Could not find a camelot jar file in the installation location")
          false)))))

(defn stop!
  [config]
  (p/catch (http/post (gstr/format "http://localhost:%d/quit"
                                   (get-in config [:server :http-port])))
      (fn [e] (log/log :error (.-message e)))))
