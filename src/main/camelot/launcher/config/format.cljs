(ns camelot.launcher.config.format
  (:require [cljs.reader :as reader]))

(defn- to-number
  "Convert the value at `path` to a number (hopefully)."
  [path config]
  (update-in config path #(if (string? %)
                            (reader/read-string %)
                            %)))

(defn- to-keyword
  "Convert the value at path to a keyword"
  [path config]
  (update-in config path keyword))

(defn- empty-to-nil
  "Convert empty values (e.g., empty string) to nil."
  [path config]
  (update-in config path
             #(when (or (not (or (coll? %) (string? %)))
                        (seq %))
                %)))

(def ^:private transformers
  [{:path [:server :http-port]
    :transform to-number}
   {:path [:language]
    :transform to-keyword}
   {:path [:species-name-style]
    :transform to-keyword}
   {:path [:server :jvm-extra-args]
    :transform empty-to-nil}
   {:path [:server :max-heap-size]
    :transform #(->> %2 (to-number %1)(empty-to-nil %1))}])

(defn- apply-transforms
  "Apply all transformers to `config``."
  [config]
  (reduce (fn [acc {:keys [path transform]}]
            (transform path acc))
          config
          transformers))

(defn- elide-empty
  "Remove keys for empty values from `m`, recursively."
  [m]
  (letfn [(reducer [acc k v]
            (cond
              (nil? v)
              acc

              (map? v)
              (let [nv (elide-empty v)]
                (if (empty? nv)
                  acc
                  (assoc acc k nv)))

              (and (seq? v) (seq v))
              (assoc acc k v)

              :else
              (assoc acc k v)))]
    (reduce-kv reducer {} m)))

(defn transform
  "Transform `config` to ensure it matches the format expected by `camelot.market.config`.
  May differ slightly due to serialisation/deserialisation."
  [config]
  (->> config
       apply-transforms
       elide-empty))
