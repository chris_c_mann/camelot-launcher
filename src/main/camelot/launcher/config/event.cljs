(ns camelot.launcher.config.event)

(defn config-read
  [config]
  {:type :config-read-event
   :configuration config})

(defn config-read-failure
  [error-type configuration validation-failure problems]
  {:type :config-read-failure-event
   :configuration configuration
   :problems problems
   :error {:type error-type
           :message validation-failure}})

(defn config-save-success
  [config]
  {:type :config-save-success-event
   :configuration config})

(defn config-save-failure
  [problems config]
  {:type :config-save-failure-event
   :problems problems
   :configuration config})
