(ns camelot.launcher.config.core
  "Configuration management for Camelot."
  (:require [camelot.market.config :as market-config]
            [camelot.launcher.common.events :as common-events]
            [camelot.launcher.config.format :as format]
            [camelot.launcher.config.event :as config-event]
            [camelot.launcher.event.core :as event]
            [clojure.spec.alpha :as s]
            [cljs.core.async :as async :include-macros true]
            [integrant.core :as ig]))

(def module-key :config)
(def module (hash-map module-key {:config/event (ig/ref event/module-key)}))

(defn read-config
  [system]
  (let [config (market-config/read-config)]
    (reset! (get system :config) config)
    @(get system :config)))

(defn reset-config!
  [system]
  (market-config/reset-config!)
  (read-config system))

(defn- save!
  [system config]
  (market-config/write-config! (format/transform config))
  (read-config system))

(defn handle-event
  [system event]
  (condp = (:type event)
    :config-read-request-event
    (let [config (read-config system)]
      (async/put! (::broadcast-channel-tx system)
                  (if (s/valid? ::common-events/configuration config)
                    (config-event/config-read config)
                    (config-event/config-read-failure
                     :invalid-configuration
                     config
                     (s/explain-str ::common-events/configuration config)
                     (market-config/validate-config config)))))

    :config-reset-request-event
    (let [config (reset-config! system)]
      (async/put! (::broadcast-channel-tx system)
                  (if (s/valid? ::common-events/configuration config)
                    (config-event/config-read config)
                    (config-event/config-read-failure
                     :invalid-configuration
                     config
                     (s/explain-str ::common-events/configuration config)
                     (market-config/validate-config config)))))

    :config-save-request-event
    (let [_ (:configuration event)]
      (try
        (save! system (:configuration event))
        (async/put! (::broadcast-channel-tx system)
                    (config-event/config-save-success @(get system :config)))
        (catch js/Error e
          (let [problems (set (:problem-paths (ex-data e)))
                config (:config (ex-data e))]
            (async/put! (::broadcast-channel-tx system)
                        (config-event/config-save-failure
                         problems
                         config))))))

    nil))

(defn- start-listener!
  [system]
  (let [ch (::broadcast-channel-rx system)]
    (async/go-loop []
      (let [event (async/<! ch)]
        (handle-event system event))
      (recur))
    ch))

(defmethod ig/prep-key module-key [_ system]
  (assoc system
         :config (atom nil)
         ::broadcast-channel-tx (async/chan)))

(defmethod ig/init-key module-key [_ system]
  (let [broadcast-ch-rx ((-> system :config/event :register!)
                         (::broadcast-channel-tx system))
        s (assoc system ::broadcast-channel-rx broadcast-ch-rx)]
    (read-config s)
    (start-listener! s)
    s))
