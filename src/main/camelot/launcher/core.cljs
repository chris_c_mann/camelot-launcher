(ns camelot.launcher.core
  (:require [camelot.launcher.process.core :as process]
            [camelot.launcher.config.core :as config]
            [camelot.launcher.environ.core :as environ]
            [camelot.launcher.electron.core :as electron]
            [camelot.launcher.event.core :as event]
            [camelot.launcher.ui.core :as ui]
            [camelot.launcher.logs.core :as logs]
            [camelot.launcher.common.error :as error]
            [integrant.core :as ig]
            [cljs.nodejs :as nodejs]))

(nodejs/enable-util-print!)

(def uncaught-exception-event "uncaughtException")

(defn- electron
  []
  (js/require "electron"))

(def dialog (.-dialog (electron)))
(defonce process (js/require "process"))

(defn- exception-handler
  [e]
  (.showErrorBox dialog "An error occurred in the main process"
                 (error/stringify-error-chain e)))

(defn- register-exception-handler!
  []
  (.on process uncaught-exception-event exception-handler))

(def system-config
  (merge config/module
         event/module
         electron/module
         environ/module
         process/module
         ui/module
         logs/module))

(defn start!
  []
  (register-exception-handler!)
  (-> system-config ig/prep ig/init))

(defn stop!
  [system]
  (ig/halt! system))

(defn -main
  [& args]
  (start!))

(set! *main-cli-fn* -main)
