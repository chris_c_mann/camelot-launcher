(ns camelot.launcher.logs.event)

(defn logs-read-failure
  [error]
  {:type :logs-read-failure-event
   :error {:type :read-error
           :message (.toString error)}})

(defn logs-read-success
  [session-contents process-contents]
  {:type :logs-read-success-event
   :contents {:session session-contents
              :process process-contents}})
