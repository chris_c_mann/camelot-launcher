(ns camelot.launcher.renderer.view.components.common
  (:require [reagent.core :as reagent]))

(defn spinner
  []
  [:div {:className "align-center"}
   [:img {:className "spinner"
          :alt "Loading..."
          :src "images/spinner.gif"}]])

(defn spinner-small
  []
  [:div {:className "align-center"}
   [:img {:className "spinner-small"
          :alt "Loading..."
          :src "images/spinner.gif"}]])

(defn phased-spinner
  []
  (let [phase (reagent/atom 0)]
    (.setTimeout js/window #(swap! phase inc) 1500)
    (.setTimeout js/window #(swap! phase inc) 30000)
    (fn []
      [:div
       [spinner]
       [:p {:className "spinner-text subtle-text"}
        (condp = @phase
          0 ""

          1
          "This might take a moment..."

          "This is taking longer than expected. Check the system logs for more information.")]])))
