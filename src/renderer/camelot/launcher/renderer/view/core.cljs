(ns camelot.launcher.renderer.view.core
  (:require
   [camelot.launcher.renderer.subs.core :as subs]
   [camelot.launcher.renderer.view.application.core :as application]
   [camelot.launcher.renderer.view.server.core :as server]
   [camelot.launcher.renderer.view.logs.core :as logs]
   [camelot.launcher.renderer.view.config.core :as config]
   [camelot.launcher.renderer.view.datasets.core :as datasets]
   [camelot.launcher.renderer.view.components.common :as common-components]
   [re-frame.core :as re-frame]))

(def menu
  [{:concept :main
    :name "Camelot"}
   {:concept :datasets
    :name "Datasets"}
   {:concept :config
    :name "Settings"}
   {:concept :logs
    :name "System logs"}])

(defn change-route
  [route]
  (re-frame/dispatch [:navigation-route-change-event
                      {:next-route [route]}]))

(defn concept-item
  [{:keys [route active-route name]}]
  [:button {:className (str "menu-item "
                         (if (= route active-route) "active" ""))
            :onClick #(change-route route)}
   (:span {:className "menu-item-title"}
          name)])

(defn menu-component
  []
  (let [route (first @(re-frame/subscribe [::subs/route]))]
    [:div {:className "section simple-menu"}
     (doall (for [concept menu]
              [concept-item {:key (:concept concept)
                             :active-route route
                             :route (:concept concept)
                             :name (:name concept)}]))]))

(defn menu-router-component
  []
  (re-frame/dispatch [:config-read-request-event])
  (fn []
    (let [route @(re-frame/subscribe [::subs/route])]
      (condp = (first route)
        :main [server/server-section]
        :config [config/config-panel]
        :datasets [datasets/datasets-panel]
        :logs [logs/section]
        :application [application/webview]
        nil))))

(defn main
  []
  (let [initialised? @(re-frame/subscribe [::subs/ipc-initialised?])]
    [:div {:className "main-content-container"}
     [:div {:className "main-content"}
      [:div {:className "intro"}
       [:h2 "Welcome to Camelot"]]
      [:div {:className "split-menu"}
       [:div {:className "section-container"}
        [menu-component]]
       [:div {:className "section-container"}
        (if initialised?
          [menu-router-component]
          [:div {:className "section"}
           [common-components/spinner]])]]]]))
