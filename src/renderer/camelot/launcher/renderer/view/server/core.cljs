(ns camelot.launcher.renderer.view.server.core
  (:require
   [camelot.launcher.renderer.view.components.common :as common-components]
   [camelot.launcher.renderer.subs.process :as subs]
   [re-frame.core :as re-frame]))

(defn to-application
  []
  (re-frame/dispatch [:navigation-route-change-event
                      {:next-route [:application]}]))

(defn status-class
  [status]
  (condp = status
    :started "status-green"
    :stopped "status-red"
    "status-grey"))

(defn start-button
  []
  (let [status @(re-frame/subscribe [::subs/process-state])]
    [:div {:className "button-container"}
     (if (= status :stopped)
       [:button {:className "btn btn-primary"
                 :on-click #(re-frame/dispatch
                             [:process-state-change-request-event :started])}
        "Start"]
       [:button {:className "btn btn-default"
                 :on-click #(re-frame/dispatch
                             [:process-state-change-request-event :stopped])
                 :disabled (not= status :started)}
        "Stop"])
     [:button {:className (str "btn " (if (= status :started) "btn-primary" "btn-default"))
               :on-click to-application
               :disabled (not= status :started)}
      "Connect"]]))

(defn status-field
  []
  (let [status @(re-frame/subscribe [::subs/process-state])
        process-state-description @(re-frame/subscribe
                                    [::subs/process-state-description])]
    [:div
     [:label {:className "field-label"}
      "Status"]
     [:p {:className (str "status " (status-class status))}
      process-state-description]
     [start-button]]))

(defn runtime-info-component
  []
  (let [alternate-urls @(re-frame/subscribe [::subs/alternate-urls])
        software-version @(re-frame/subscribe [::subs/software-version])
        database-version @(re-frame/subscribe [::subs/database-version])
        java-version @(re-frame/subscribe [::subs/java-version])
        max-heap-size @(re-frame/subscribe [::subs/max-heap-size])]
    (if (not software-version)
      [common-components/spinner]
      [:div
       [:div
        [:label {:className "field-label"}
         "Camelot version"]
        [:p software-version]]
       [:div
        [:label {:className "field-label"}
         "Database version"]
        [:p database-version]]
       [:div
        [:label {:className "field-label"}
         "Java version"]
        [:p java-version]]
       [:div
        [:label {:className "field-label"}
         "Max available memory (heap size)"]
        [:p (str (/ max-heap-size 1024 1024) " MB")]]
       [:div
        [:label {:className "field-label"}
         "Known URLs"]
        [:ul
         (doall (map (fn [url]
                       [:li {:key url}
                        [:a {:href url :target "_blank"} url]])
                     alternate-urls))]]])))

(defn status-component
  []
  (let [status @(re-frame/subscribe [::subs/process-state])]
    [:div {:className "scroll-container"}
     [:div {:className "server-info-panel"}
      [status-field]
      (condp = status
        :started
        [runtime-info-component]

        :stopped
        nil

        [common-components/phased-spinner])]]))

(defn server-section
  []
  (re-frame/dispatch [:process-state-notification-request-event])
  (fn []
    [:div {:className "section"}
     [status-component]]))
