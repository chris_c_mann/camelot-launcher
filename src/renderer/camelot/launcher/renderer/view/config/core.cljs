(ns camelot.launcher.renderer.view.config.core
  (:require
   [camelot.launcher.renderer.view.config-form.core :as config-form]
   [camelot.launcher.renderer.view.config.options :as options]
   [camelot.launcher.renderer.subs.config :as subs]
   [camelot.launcher.renderer.view.components.common :as common-components]
   [cljs-http.client :as http]
   [cljs.core.async :as async :include-macros true]
   [re-frame.core :as re-frame]
   [reagent.core :as reagent]))

(def reset-message
  "This will reset all configuration options to Camelot's defaults.\n\nThis includes dataset configuration.\n\nAre you sure you wish to continue?")

(defn reset-config-dispatcher
  []
  (when (js/confirm reset-message)
    (re-frame/dispatch [:config-reset-request-event])))

(defn button-container
  []
  (let [saving? @(re-frame/subscribe [::subs/config-saving?])]
    [:div {:className "flex-end"}
     [:div {:className "button-container"}
      [:button {:className "btn btn-primary"
                :on-click #(re-frame/dispatch [:config-save-request-event])
                :disabled saving?}
       "Save"]
      [:button {:className "btn btn-dangerous"
                :on-click reset-config-dispatcher
                :disabled saving?}
       "Reset to defaults"]]]))

(defn- camelot-detector-separator
  []
  [:div
   [:hr]
   [:h4 "Animal detection"]
   [:div {:className "banner info"}
    [:p
     "Process images faster by auto-detecting those with animals. "
     "This is an online service and requires separate registration. "
     [:a {:href "https://www.camelotproject.org/register/" :target "_blank" :rel "noopener noreferrer"} "Register"]]]])

(defn- advanced-settings-separator
  []
  [:div
   [:hr]
   [:h4 "Advanced settings"]
   [:div {:className "banner warning"}
    [:p
     [:strong "Caution: "]
     "please refer to the " [:a {:href "https://camelot-project.readthedocs.io/en/latest/administration.html#advanced-settings" :target "_blank" :rel "noopener noreferrer"} "documentation"] " before changing these settings. The settings below are intended for advanced use cases and may have surprising results."]]])

(defn- camelot-detector-check-authentication-button
  []
  (let [result (reagent/atom {:status :unknown})]
    (fn []
      (let [config @(re-frame/subscribe [::subs/config])
            {:keys [api-url username password]} (:detector config)]
        [:div
         [:button
          {:className "btn btn-primary"
           :onClick #(do
                       (reset! result {:status :checking})
                       (async/go
                         (let [resp (async/<! (http/post (str api-url "/account/auth")
                                                         {:basic-auth {:username username
                                                                       :password password}}))]
                           (reset! result resp))))}
          "Check login"]
         (condp = (:status @result)
           :unknown
           ""

           :checking
           [:div {:className "detector-authentication-status"}
            [common-components/spinner-small]]

           204
           [:div {:className "detector-authentication-status success"}
            "Success!"]

           401
           [:div {:className "detector-authentication-status failure"}
            "Invalid username and password"]

           [:div {:className "detector-authentication-status failure"}
            "Problem while checking login"])]))))

(defn render-form
  []
  (let [problem? @(re-frame/subscribe [::subs/any-problem?])]
    [:div {:className "flex-column"}
     [:h3 "Settings"]
     [:div {:className (if problem? "validation-error" "")}
      (when problem? "There was a problem with the configuration")]
     [config-form/form {:options options/config-options
                        :element-type-components
                        {:advanced-settings-separator advanced-settings-separator
                         :detector/separator camelot-detector-separator
                         :detector/check-authentication-button camelot-detector-check-authentication-button}}]
     [button-container]]))

(defn config-panel
  []
  (re-frame/dispatch [:config-read-request-event])
  (fn []
    [:div {:className "section"}
     (if @(re-frame/subscribe [::subs/config-loaded?])
       [render-form]
       [common-components/spinner])]))
