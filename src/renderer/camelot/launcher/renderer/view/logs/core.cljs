(ns camelot.launcher.renderer.view.logs.core
  (:require
   [camelot.launcher.renderer.subs.logs :as subs]
   [camelot.launcher.renderer.view.components.common :as common-components]
   [re-frame.core :as re-frame]
   [reagent.core :as reagent]))

(defn render-pre
  [& children]
  [:div {:className "flex-column"}
   [:pre children]])

(defn log-panel
  []
  (let [selected (reagent/atom :session)]
    (fn []
      (let [process-log @(re-frame/subscribe [::subs/process-log])
            session-log @(re-frame/subscribe [::subs/session-log])]
        [:div
         [:h3 "System logs"]
         [:ul {:className "tabs"}
          [:li {:className (str "tab" (if (= @selected :session) " active" ""))}
           [:a {:href "#" :onClick #(reset! selected :session)}
            "Launcher"]]
          [:li {:className (str "tab" (if (= @selected :process) " active" ""))}
           [:a {:href "#" :onClick #(reset! selected :process)}
            "Server"]]]
         [render-pre (if (= @selected :session)
                       session-log
                       process-log)]]))))

(defn section
  []
  (re-frame/dispatch [:logs-read-request-event])
  (fn []
    [:div {:className "section-wrapper"}
     [:div {:className "section extra-wide-section"}
      (if @(re-frame/subscribe [::subs/logs-loaded?])
        [log-panel]
        [common-components/spinner])]]))
