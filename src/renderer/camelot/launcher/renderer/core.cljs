(ns camelot.launcher.renderer.core
  (:require
   [camelot.launcher.renderer.view.core :as view]
   [camelot.launcher.renderer.events.core :as events]
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]))

(enable-console-print!)

(defn main
  []
  (events/register!)
  (re-frame/dispatch-sync [::events/initialize-db])
  (re-frame/clear-subscription-cache!)
  (rdom/render [view/main]
               (.getElementById js/document "app")))

(defonce init
  (main))
