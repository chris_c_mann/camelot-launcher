(ns camelot.launcher.renderer.subs.config
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::config
 (fn [db [_ path]]
   (get-in db `[:config :buffer ~@path])))

(re-frame/reg-sub
 ::config-loaded?
 (fn [db _]
   (not (nil? (get-in db [:config :buffer])))))

(re-frame/reg-sub
 ::problem?
 (fn [db [_ path]]
   (contains? (get-in db [:config :problems]) path)))

(re-frame/reg-sub
 ::any-problem?
 (fn [db [_ path]]
   (not (empty? (get-in db [:config :problems])))))

(re-frame/reg-sub
 ::config-saving?
 (fn [db _]
   (true? (get-in db [:config :saving?]))))

(re-frame/reg-sub
 ::server-http-port
 (fn [db _]
   (get-in db [:config :buffer :server :http-port])))

(re-frame/reg-sub
 ::datasets
 (fn [db _]
   (get-in db [:config :datasets])))

(re-frame/reg-sub
 ::dataset-name
 (fn [db [_ dataset]]
   (println db (get-in db [:config :buffer]))
   (get-in db [:config :buffer :datasets dataset :name])))
