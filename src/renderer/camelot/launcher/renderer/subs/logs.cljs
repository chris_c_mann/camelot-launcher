(ns camelot.launcher.renderer.subs.logs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
 ::logs-loaded?
 (fn [db _]
   (not (nil? (:logs db)))))

(re-frame/reg-sub
 ::process-log
 (fn [db _]
   (get-in db [:logs :process])))

(re-frame/reg-sub
 ::session-log
 (fn [db _]
   (get-in db [:logs :session])))
