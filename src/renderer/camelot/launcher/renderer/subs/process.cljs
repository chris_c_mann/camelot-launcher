(ns camelot.launcher.renderer.subs.process
  (:require [re-frame.core :as re-frame]))

(def ^:private process-state-descriptions
  {:starting "Starting..."
   :stopping "Stopping..."
   :started "Running"
   :stopped "Stopped"
   :unknown "Unknown..."})

(defn runtime
  [db]
  (get-in db [:process :runtime]))

(re-frame/reg-sub
 ::alternate-urls
 (fn [db _]
   (-> db runtime :alternate-urls)))

(re-frame/reg-sub
 ::software-version
 (fn [db _]
   (-> db runtime :software-version)))

(re-frame/reg-sub
 ::database-version
 (fn [db _]
   (-> db runtime :database-version)))

(re-frame/reg-sub
 ::java-version
 (fn [db _]
   (-> db runtime :java :version)))

(re-frame/reg-sub
 ::max-heap-size
 (fn [db _]
   (-> db runtime :java :max-heap-size)))

(re-frame/reg-sub
 ::process-state
 (fn [db]
   (:process-state db)))

(re-frame/reg-sub
 ::process-state-description
 (fn [db]
   (process-state-descriptions (:process-state db))))
