(ns camelot.launcher.renderer.events.ipc
  (:require [camelot.launcher.common.log :as log]
            [camelot.launcher.common.json :as json]))

(def electron (js/require "electron"))
(def ipc-renderer (.-ipcRenderer electron))

(defn send
  [channel event]
  (log/log :debug "IPC => BROADCAST: " event)
  (.send ipc-renderer (name channel) (json/to-json event)))

(defn register-listener
  [channel listener]
  (.on ipc-renderer (name channel)
       (fn [_ raw-event]
         (log/log :debug "BROADCAST => IPC: " raw-event)
         (let [event (json/from-json raw-event)]
           (listener event)))))
