(ns camelot.launcher.renderer.events.fx.process
  (:require [camelot.launcher.common.events :as common-events]
            [camelot.launcher.renderer.events.ipc :as ipc]
            [re-frame.core :as re-frame]))

(defn process-command-event-fx
  [_ [_ target-state]]
  (ipc/send :renderer
            (common-events/server-state-change-request target-state))
  {})

(defn process-state-notification-request-fx
  [_ _]
  (ipc/send :renderer
            (common-events/server-state-notification-request))
  {})

(defn process-runtime-info-notification-request-fx
  [_ _]
  (ipc/send :renderer
            (common-events/server-runtime-info-notification-request))
  {})

(defn- switch-to-application-route?
  [db payload]
  (and (:process-state db)
       (not= :started (:process-state db))
       (= (:new-state payload) :started)
       (= (get-in db [:navigation :route]) [:main])))

(defn process-state-change-fx
  [{:keys [db]} [_ payload]]
  (let [next-db (assoc db :process-state (:new-state payload))]
    {:db next-db
     :dispatch-n (cond-> []
                   (switch-to-application-route? db payload)
                   (conj [:navigation-route-change-event {:next-route [:application]}])

                   (= (:new-state payload) :started)
                   (conj [:process-runtime-info-notification-request-event]))}))

(defn process-state-change-failure-fx
  [_ [_ {:keys [error]}]]
  (js/alert (:message error))
  {})

(defn- start-camelot-automatically?
  [db payload]
  (and (nil? (:process-state db))
       (= (:current-state payload) :stopped)
       (get-in db [:config :buffer :open-browser-on-startup])))

(defn process-state-notification-fx
  [{:keys [db]} [_ payload]]
  (let [next-db (assoc db :process-state (:current-state payload))]
    {:db next-db
     :dispatch-n (cond-> []
                   (start-camelot-automatically? db payload)
                   (conj [:process-state-change-request-event :started])

                   (= (:current-state payload) :started)
                   (conj [:process-runtime-info-notification-request-event]))}))

(defn process-runtime-info-notification-fx
  [{:keys [db]} [_ payload]]
  {:db (assoc-in db [:process :runtime]
                 (select-keys payload [:alternate-urls
                                       :software-version
                                       :database-version
                                       :java]))})
