(ns camelot.launcher.renderer.events.fx.navigation
  (:require [camelot.launcher.common.events :as common-events]
            [re-frame.core :as re-frame]))

(defn navigation-route-change-fx
  [cofx [_ {:keys [next-route]}]]
  {:db (assoc-in (:db cofx) [:navigation :route] next-route)})
