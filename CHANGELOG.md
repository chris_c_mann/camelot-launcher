# Changelog
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

### [Unreleased]
* Add support for electron 5.x.x
* Add options for animal detector functionality
* Add bulk import root location

### 1.5.2
#### Added
* Add hyperlinks for known URLs

#### Changed
* Rearrange settings and distinguish advanced settings with link to documentation
* Open all new windows in the system browser

### 1.5.1
#### Changed
* Improves installation location scanning for MacOS

### 1.5.0
#### Added
* Initial release
