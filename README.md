# Camelot Launcher

Application launcher and configuration tool for [Camelot](https://gitlab.com/camelot-project/camelot).

## Development

Build and start a development environment by running:

```
npm install
script/dev
```

And then, in a separate terminal:

```
script/run
```

## License

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
